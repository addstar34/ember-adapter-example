import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  extractId(modelClass, resourceHash) {
    if (resourceHash.hasOwnProperty('AssessmentID')) {
      return resourceHash.AssessmentID;
    }
    return this._super(modelClass, resourceHash);
  }
});
