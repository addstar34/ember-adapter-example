import DS from 'ember-data';
import { v4 } from 'ember-uuid';

export default DS.RESTAdapter.extend({
  host: 'https://INSERT-UID-HERE.execute-api.us-east-1.amazonaws.com',
  namespace: 'prod/assessments',

  urlForQuery(_query, _modelName) {
    return `${this.host}/${this.namespace}/getadsassessments`
  },

  handleResponse(status, headers, payload, requestData) {
    let locales = [];
    const localeIds = [];
    const assessments = payload.RequestAssessmentList.Assessments.map((a) => {
      const assessment = a.Assessment;

      locales = locales.concat(assessment.Locales.map((locale) => {
        const id = v4();
        localeIds.push(id);

        return Object.assign({ id }, locale.Locale )
      }));

      delete assessment.Locale;
      assessment.locales = localeIds;
      return assessment;
    })

    payload = { assessment: assessments, locales }

    return this._super(status, headers, payload, requestData);
  }
});
