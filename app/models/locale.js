import DS from 'ember-data';

const { attr, belongsTo } = DS;

export default DS.Model.extend({
  assessment: belongsTo('assessment'),

  localeName: attr('string'),
  testName: attr('string'),
  type: attr('string'),
});
